from paraview.simple import *
from paraview import print_info

# catalyst options
from paraview.catalyst import Options
options = Options()

# print start marker
print_info("begin '%s'", __name__)

# directory under which to save all extracts
# generated using Extractors defined in the pipeline, if any.
# (optional, but recommended)
options.ExtractsOutputDirectory = '/tmp'
SaveExtractsUsingCatalystOptions(options)

view = CreateRenderView()
producer = TrivialProducer(registrationName='fides')
display = Show(producer)
view.ResetCamera()
ColorBy(display, ('POINTS', 'density'))
display.RescaleTransferFunctionToDataRange(True, False)
display.SetScalarBarVisibility(view, True)
transFunc = GetColorTransferFunction('density')
transFunc.RescaleOnVisibilityChange = 1

extractor = CreateExtractor('PNG', view, registrationName='PNG1')
extractor.Writer.FileName = 'output-{timestep}.png'
extractor.Writer.ImageResolution = [800, 800]

def catalyst_initialize():
    print_info("in '%s::catalyst_initialize'", __name__)

def catalyst_execute(info):
    print_info("in '%s::catalyst_execute'", __name__)
    global producer

    print("-----------------------------------")
    print("executing (cycle={}, time={})".format(info.cycle, info.time))
    producer.UpdatePipeline()
    print("bounds:", producer.GetDataInformation().GetBounds())
    print(producer.PointData.GetNumberOfArrays())
    print("density-range:", producer.PointData['density'].GetRange(0))


def catalyst_finalize():
    print_info("in '%s::catalyst_finalize'", __name__)

# print end marker
print_info("end '%s'", __name__)
