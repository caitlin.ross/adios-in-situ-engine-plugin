#include <iostream>
#include <vector>

#include <adios2.h>
#include <mpi.h>

int main(int argc, char *argv[])
{
    unsigned long N = 256;
#ifdef ADIOS2_HAVE_MPI
    MPI_Init(&argc, &argv);
    adios2::ADIOS adios(MPI_COMM_WORLD);
#else
    adios2::ADIOS adios;
#endif

    adios2::IO io = adios.DeclareIO("test");
    io.SetEngine("plugin");
    adios2::Params params;
    params["PluginName"] = "fides";
    params["PluginLibrary"] = "FidesInSituPlugin";
    params["DataModel"] = "/home/local/KHQ/caitlin.ross/dev/adios2/adios-in-situ-engine-plugin/cartesian.json";
    params["Script"] = "/home/local/KHQ/caitlin.ross/dev/adios2/adios-in-situ-engine-plugin/fides-script.py";
    io.SetParameters(params);

    auto u = io.DefineVariable<double>("density", {N, N, N}, {0, 0, 0}, {N, N, N});
    adios2::Engine writer = io.Open("writer", adios2::Mode::Write);
    for (int64_t timeStep = 0; timeStep < 2; ++timeStep)
    {
        writer.BeginStep();
        std::vector<double> v(N * N * N, 3.2);
        std::cout << "Putting data at address   " << v.data()
                  << " into inline writer.\n";
        writer.Put(u, v.data());
        writer.EndStep();
    }
    MPI_Finalize();
    return 0;
}

